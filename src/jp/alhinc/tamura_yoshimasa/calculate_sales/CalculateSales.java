package jp.alhinc.tamura_yoshimasa.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class CalculateSales {



	public static void main (String[] args){



		//エラー処理（コマンドラインの数）
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		HashMap<String, String> mapCode = new HashMap<String, String>();
		HashMap<String, Long> mapManey = new HashMap<String, Long>();

		//コマンドライン引数、ファイル名、フォーマットの正規表現、ファイルタイプ、マップ１、マップ２
		if(!fileRead(args[0] , "branch.lst" ,"^[0-9]{3}$" ,"支店定義",mapCode ,mapManey)){
			return;
		}

		//コマンドライン引数、ファイル名の正規表現、、ファイルタイプ、コードタイプ、ファイル内のコンテンツ数、マップ２
		if(!fileCollect(args[0] ,"^[0-9]{8}(.rcd)$" ,"売上" ,"支店" ,2 ,mapManey)){
			return;
		}

		//コマンドライン引数、ファイル名、マップ１、マップ２
		if(!fileAdd(args[0], "branch.out" ,mapCode ,mapManey)){
			return;
		}

	}
	//メソッド作成ーーーーーーーーーーーーーーーーーーーーーーーーーー

	private static boolean fileRead(String path ,String fileName,String formatRegex ,String fileType
			,HashMap<String, String> mapCode ,HashMap<String, Long> mapManey){
		//１支店定義ファイルからデータ読み込みーーーーーーーーーーー

		BufferedReader brMain = null;
		try {

			File fMain = new File(path ,fileName);
			//エラー処理（支店定義ファイルなし）
			if(!fMain.exists()){
				System.out.println(fileType + "ファイルが存在しません");
				return false;
			}
			brMain = new BufferedReader(new FileReader(fMain));

			String mainLain;
			while((mainLain = brMain.readLine()) != null ){
				//エラー処理（支店定義ファイルのフォーマット不正）
				String[] fs = mainLain.split(",", -1);
				if((fs.length ==2) && (fs[0].matches(formatRegex))){
					mapCode.put(fs[0], fs[1]);
					mapManey.put(fs[0],0l);
				}else{
					System.out.println(fileType + "ファイルのフォーマットが不正です");
					return false;

				}

			}


		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally{
			if(brMain != null){
				try{
					brMain.close();
				}catch(IOException e ){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


	private static boolean fileCollect(String path  ,String fileNameRegex ,String fileType ,String codeType
			,int contentSize ,HashMap<String, Long> mapMoney){
		BufferedReader brMoney = null;

		//Fileクラスのオブジェクト生成
		File fMoney = new File(path);
		//一覧を取得
		File[] fMoneys = fMoney.listFiles();
		//選別リスト
		ArrayList<File> lfile = new ArrayList<File>();//選別リスト格納用
		for(int i=0; i<fMoneys.length; i++){
			String str = fMoneys[i].getName();

			//ファイル選択（8桁の.rcd拡張子 かつ ファイルであれば）
			if((str.matches(fileNameRegex)) && (fMoneys[i].isFile())){
				lfile.add(fMoneys[i]);
			}
		}

		//エラー処理（連番確認）
		ArrayList<Integer> Lnum = new ArrayList<Integer>();//売り上げファイルネームの数を格納用
		for(int i=0; i<lfile.size(); i++){
			String fileName = lfile.get(i).getName();
			String fnum = fileName.substring(0,8);
			int ifnum = Integer.parseInt(fnum);
			Lnum.add(ifnum);
		}
		for(int i=0; i<Lnum.size()-1; i++){
			if(Lnum.get(i+1) != Lnum.get(i)+1){
				System.out.println(fileType + "ファイル名が連番になっていません");
				return false;
			}
		}

		//厳選ファイル数だけ回す
		for(int i=0; i<lfile.size(); i++){
			try{
				ArrayList<String> listContent = new ArrayList<String>();//支店コードと売上額用
				brMoney = new BufferedReader(new FileReader(lfile.get(i)));

				//ファイルの中身を抽出、保存
				String moneyLain;
				while((moneyLain = brMoney.readLine()) != null ){
					listContent.add(moneyLain);
				}
				//エラー処理（該当コード該当なし）
				if(!mapMoney.containsKey(listContent.get(0))){
					System.out.println(lfile.get(i).getName() + "の" + codeType + "コードが不正です");
					return false;
				}
				//エラー処理（該当ファイルフォーマット不正）
				if(listContent.size() != contentSize){
					System.out.println(lfile.get(i).getName() + "のフォーマットが不正です");
					return false;
				}

				long a = mapMoney.get(listContent.get(0));//コードの現時点の金額
				//エラー処理（売り上げ金額に数字以外が含まれたとき）
				if(!listContent.get(1).matches("^[0-9]+$")){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				long b = Long.parseLong(listContent.get(1));//追加したい金額//Long型に変換
				long c = a+b;

				//エラー処理（金額超過）
				if(c >= 10000000000l){
					System.out.println("合計金額が10桁を超えました");
					return false;
				}

				mapMoney.put(listContent.get(0),c);
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} finally{
				if(brMoney != null){
					try{
						brMoney.close();
					}catch(IOException e ){
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}

		}
		return true;

	}

	private static boolean fileAdd(String path , String fileName
			,HashMap<String, String> mapCode ,HashMap<String, Long> mapManey){
		BufferedWriter bw = null;
		try{
			//新規ファイル作成
			File newfile = new File(path, fileName);
			newfile.createNewFile();
			FileWriter fw= new FileWriter(newfile);
			bw = new BufferedWriter(fw);
			//key数の数だけ回す
			for (String key : mapCode.keySet()) {
				bw.write(key + "," + mapCode.get(key) + "," + mapManey.get(key) );
				bw.newLine();//改行
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally{
			if(bw != null){
				try{
					bw.close();
				}catch(IOException e ){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return  true;
	}

}

